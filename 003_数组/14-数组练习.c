#include <stdio.h>

int maxOfArray(int[] , int);

int main() {
    
    int a[] = {10, 11, 3, 5, 89, 56};
    
    int max = maxOfArray(a, sizeof(a)/sizeof(int));
    
    printf("max is %d\n",max);
    
    return 0;
    
}

/*
 设计一个函数,用来找某个整型数组中的最大值
 */
int maxOfArray(int array[], int count) {
   // 默认第0个元素最大
    int max = array[0];
    
    for (int i = 1; i < count; i++) {
        
        if(array[i] > max) {
            
            max = array[i];
        }
        
//        printf("array[%d]=%d\n",i,array[i]);
        
    }
    
    return max;
    
    
}







