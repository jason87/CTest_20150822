
#include <stdio.h>
void change2(int []);

int main() {
    
    int ages[5] = {10, 11, 14, 15, 17};
    
//    printf("数组占用的字节数:%d\n",sizeof(ages));  20字节
    
    change2(ages);// 代表地址 访问的存储空间地址一样
    
    printf("ages[1]=%ld\n",ages[1]);
    
    return 0;
}


// 当数组作为函数形参时,数组元素个数可以省略
// 当数组作为函数形参时,传递的数组的地址,array和ages共用一块存储空间
// 在函数内部计算形参数组所占用字节数时,永远是8(64bit) 因为系统会将形参数组转为指针类型, 指针类型永远只占8个字节
void change2(int array[]) {
    
    printf("数组占用的字节数:%d\n",sizeof(array));  // 8字节
    
    array[1] = 100;
    
}