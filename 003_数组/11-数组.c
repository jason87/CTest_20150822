/*
 数组的使用
*/

#include <stdio.h>
int main() {
    



    
    
    return 0;

}

/*
 数组在内存中存储的情况
 */
void test3() {
    
    int  ages[3];
    // 数组名就代表的数组的地址
    // ages可以看做是一个常量
    
    printf("数组的地址: %p\n",ages);
    printf("a[0]的地址: %p\n",&ages[0]);
    printf("a[1]的地址: %p\n",&ages[1]);
    printf("a[2]的地址: %p\n",&ages[2]);
}

/*
 数组的遍历
 */

void test2() {
    int ages[5] = {18, 90, 57, 47, 56};
    //    char names[5] = {[3] = 'm', 'j'};
    //
    //    printf("names[3]=%c\n names[4]=%c\n", names[3],names[4]);
    
    
    //    int length = sizeof(ages);
    //
    //    printf("length=%d\n",length);
    
    //    printf("%d\n",ages[3]);
    //
    //    printf("%d\n",ages[5]); // c语言里面不会报错 角标越界
    
    
    int length = sizeof(ages)/sizeof(int);
    
    for (int i = 0; i < length; i++) {
        printf("ages[%d]=%d\n",i,ages[i]);
    }
}


/*
  数组的简单使用
 */
void test1() {
    
    
    int age = 100;
    
    // 数组中的每一个数据成为:元素
    // 定义了一个能存放10个
    int ages[10];
    
    // 数组会为每一个元素都定义一个唯一的下标(索引),索引都是从0开始递增
    ages[0] = 20;
    ages[1] = 18;
    ages[9] = 19;
    
    printf("第0个元素的值是%d\n",ages[0]);
}