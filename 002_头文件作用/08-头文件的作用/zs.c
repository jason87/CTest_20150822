/*
 张三负责:编写main 函数
 
    1.  #include指令不能包含ls.c
    2.  一个函数只能定义一次，但是可以声明无限次
    3.  函数的定义写在.c文件中,函数的声明写在.h文件
 
*/

//#include "ls.c" 思路不可取
//#include "ww.c"
//#include <stdio.h>




//int main() {
//    
////    long r = sum(10, 11);
//    
//    long r = average(10, 11);
//    
////    printf("10+11=%ld\n",r);
//    printf("%f\n",r);
//    
//    return 0;
//}

/*---------------------------------->*/

#include <stdio.h>
// 这里有李四的函数声明
#include "lssm.c"

#include "wwsm.c"

int main() {
    
    long d = sum(10, 11);
    
    double d1 = average(10, 11);
    
    printf("和是%ld, 平均数是%f\n", d, d1);
    return 0;
    
}




























