

// 给int类型起了一个别名：MyInt
typedef int MyInt;

typedef MyInt Integer;

// 创造了一种新的类型：String1
typedef char * String1;

#define String2  char *
void test4();
int main()
{
    //test4();
    
    /*
    enum Season {spring, summer, autumn, winter};
    
    enum Season s = spring;
    
    enum Season s2 = summer;
     */
    
    /*
    enum Season {spring, summer, autumn, winter};
    
    typedef enum Season MyS;
    
    MyS s = spring;
    MyS s2 = summer;
     */
    
    // Season这个名称可以省略
    typedef enum Season {
        spring,
        summer,
        autumn,
        winter
    } MyS;
    
    MyS s = spring;
    MyS s2 = summer;
    
    
    
    
    
    return 0;
}

/*
 结构体
 */
void test4()
{
    // Date可以省略
    typedef struct Date
    {
        int year;
    } * DateP;
    
    //struct Date date = {2000};
    
    DateP p = &date;
    
    printf("year=%d\n", p->year);
    
    //struct Date *p;
    
    
    
    // Student这个名称可以省略
    typedef struct Student
    {
        int age;
        int no;
    } Stu;
    
    
    Stu stu1 = {10, 1};
    
    printf("age=%d  no=%d\n", stu1.age, stu1.no);
}

/*
    结构体类型起一个别名
  */
void test3()
{
    struct Student
    {
        int age;
        int no;
    };
    // 给结构体类型起一个别名：Stu
    typedef struct Student Stu;
    
    Stu stu1 = {10, 1};
    
    Stu stu2;
    
    printf("age=%d  no=%d\n", stu1.age, stu1.no);
}


/*
 typedef和#define的区别
 */
void test2()
{
    //char *name = "jack";
    //char *name2 = "rose";
    
    /*
     String1 name = "jack";
     
     String1 name2 = "rose";
     
     String2 name3 = "jim";
     
     String2 name4 = "mj";
     */
    
    /*
     String1 name, name2;
     相当于下面的代码
     String1 name;
     String1 name2;
     
     String2 name3, name4;
     相当于下面的代码
     char *name3, name4;
     相当于下面的代码
     char *name3;
     char name4;
     */
    
    
    //printf("%s\n%s", name, name3);
    
    // 连续定义两个int类型的变量
    // int a, b;
    
    // 定义一个指针变量a，定义一个int类型的变量b
    /*
     int *a, b;
     相当于下面的代码
     int *a;
     int b;
     */
}

void test1()
{
    int a = 10;
    
    MyInt b = 11;
    
    Integer i = 20;
    
    Integer a1, a2;
}