
#include <stdio.h>
// strlen 函数声明在string.h中
#include <string.h>

/*
 %d 十进制整数 int
 %ld long
 %f 浮点型(float double)
 %c 字符
 %s 字符串 (本质接受的是地址)
 
 */
int main() {
    
    /*
     '\0'的重要性
     */

    char name2[] = "haha";
    // 假设这个字符串不小心漏掉了一个'\0'  没有\0可能找到别的内存的数据
    char name[] = {'j','a'};
    
    printf("%s\n",name); // jahaha  直到遇到\0 为止
    
    printf("%s\n",&name[1]); // ahaha 拿到a的地址值从a开始 直到遇到\0为止
    
    
    // strlen计算的并不是文字的个数,计算的是字符数(一个汉字占据3个字符)
    printf("%zd\n", strlen("呵呵123"));
    
    return 0;
    
}

/*
 字符串 \0 的简介
 */
void test3() {
    // "it"
    
    char name[10] = {'i', 't','\0','a','b','\0'};
    
    char name3[10] = "itjav\0a";
    
    char name2[10] = {'i', 't'};
    
    // 字符串长度其实是不包括\0的
    // "呵呵itjava"的长度是12
    
    // 计算字符串的长度
    
    // sizeof(name3); 计算数组的长度,并不是计算字符串的真实长度
    
    // 调用系统方法 会计算name3数组中存储字符串的长度
    // 会从name这个地址开始计算字符个数, 直到遇到'\0'为止
    //    int length = strlen(name); // 2
    
    
    // %s的本质: 从name3这个地址开始 按顺序输出字符, 直到遇到'\0'为止
    printf("name的长度%d\n",name3);
}


/*
 字符的串输出
 */
void test2() {
    
    char name[10] = "itjava";
    
    name[4] = 'S';
    
    printf("name=%s\n",name);
    
    // printf(name); 编译器会有个警告,因为默认情况下是用字符串常量作为实参

    
}

/*
 字符串的定义
 */
void test1() {
    // '\0' 也是一个字符, 它是ASCII值是0
    
    // 字符: 'a' 'A' '0' '6' '!'
    // 字符串: "it" == 'i' + 't' + '\0'
    // "itjava" 由 7 个字符组成
    // '\0' 是字符串结束的标记
    
    // C语言中的字符串可以用字符数组来存储
    
    char name[8] = "itjava";
    
    // 只是一个字符数组(它并不是字符串)
    char name2[8] = {'i', 't', 'j', 'a', 'v', 'a'};
    
    // 字符串的另一种表现形式
    char name3[8] = {'i', 't', 'j', 'a', 'v', 'a','\0'};
    
    
    char name4[10] = "哈哈";
}

