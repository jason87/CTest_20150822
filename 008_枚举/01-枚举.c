#include <stdio.h>


int main() {
    
    // 定义枚举类型的同事定义枚举变量
    enum Sex {man, wonman, unknow} s;
    
    // s2表示的性别是不详
    enum Sex s2 = unknow;
    
    enum {mon, thu, wen} x;
    
    x = xingqiyi;
    
    return 0;
    
}


/*
 枚举的定义
 */
void test1() {
    
    // 0 春天
    // 1 夏天
    // 2 秋天
    // 3 冬天
    
    // int season = 200;
    
    // 1.定义枚举类型
    // 这个枚举类型有4个常量值   都是整型常量
    enum Season {spring, summer, autumn, winter};
    
    // 2.利用定义好的枚举类型定义一个枚举变量
    enum Season s = summer;
    
    printf("%d\n", winter);
    
}




