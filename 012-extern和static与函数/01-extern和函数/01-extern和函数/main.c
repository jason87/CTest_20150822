//
//  main.c
//  01-extern和函数
//
//  Created by Jason on 15/9/20.
//  Copyright © 2015年 Jason. All rights reserved.
//


/*
 外部函数: 在本文件中定义的函数能被其他文件和本文件访问
 内部函数: 在本文件中定义的函数不允许其他文件访问,只允许本文件访问
 */


/*
 extern:
    1.定义一个外部函数
    2.声明一个外部函数
 */

#include <stdio.h>


// 完整的声明一个外部函数
int sum(int, int);

int main(int argc, const char * argv[]) {
    
    int sumResult = sum(10 , 11);
    
    printf("sumResult=%d\n",sumResult);
    
       return 0;
}
