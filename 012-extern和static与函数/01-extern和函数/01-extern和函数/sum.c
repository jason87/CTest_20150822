//
//  sum.c
//  01-extern和函数
//
//  Created by 何志勇 on 15/9/20.
//  Copyright © 2015年 Jason. All rights reserved.
//
 
#include "sum.h"

// 定义一个求和函数
// 完整地定义一个外部函数
/*
extern int sum(int a, int b) {
    
    return a + b;
} */
// 默认定义的所有函数都是外部函数, extern 可以省略不写

int sum(int a, int b) {
    
    return a + b;
}
