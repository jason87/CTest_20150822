//
//  main.c
//  01-static和函数
//
//  Created by Jason on 15/9/21.
//  Copyright © 2015年 Jason. All rights reserved.
//

/*
 外部函数的局限性:整个程序中,不允许有同名的外部函数
 内部函数:整个程序中,允许不同文件有同名的内部函数
 */

/*
 static : 定义和声明一个内部函数
 extern : 定义和声明一个外部函数
 */

#include <stdio.h>
#include "test.h"

int main(int argc, const char * argv[]) {
    
    test();

    return 0;
   }


void test() {
    
    printf("调用了main.c中的test函数...\n");
    
}

