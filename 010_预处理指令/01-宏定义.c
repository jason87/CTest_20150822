#include <stdio.h>
/*
 预处理指令:
    1.在翻译之前执行的指令为预处理指令
    2.种类:
        1>宏定义(#define)
        2>条件编译
        3>文件包含(#include)
    3.特点
        1>都是以#开头
        2>指令后面不用加分号;
        3>作用域:从书写指令的那一行开始,一直到文件结尾
        4>书写的位置随意
 
 */

// 宏定义的结构: #define 宏名 值
// 宏替换:在翻译之前,将代码里面的所有count换成4
// 宏名最好用大写,跟变量名区分开来
#define COUNT 4 // 不带参数的宏定义

int main() {
    
    // 如果COUNT(宏名)出现在字符串中,就不会进行宏替换
    char *name = "COUNT";
    
    int ages[COUNT] = {1, 2, 3, 4};
    
    for (int i = 0; i < COUNT; i++) {
        
        printf("arges[%d]=%d\n",i,ages[i]);
        
        
    }
    
    
    return 0;
}



