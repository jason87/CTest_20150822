#include <stdio.h>

#define COUNT 0

int main() {

    // 如果定义了COUNT这个宏,就编译后面的代码
#ifdef COUNT
    printf("hahahah\n");
#endif
    
    return 0;
    
}

void test1() {
    
    
    //    int a = 10;
    
    /*
     if(a == 10) {
     printf("a == 10");
     } else if(a == 0) {
     printf("a == 0");
     } else {
     printf("a是其他值");
     }
     */
    
#if (COUNT == 10)
    printf("COUNT == 10");
#elif (COUNT == 0)
    printf("COUNT == 0");
#else
    printf("COUNT是其他值");
#endif
    
    
}