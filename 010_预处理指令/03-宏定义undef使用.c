#include <stdio.h>

#define PI 3.14

int main() {
    
    double d2 = 2 * PI * 10;
    // 取消定义PI这个宏,从第9行开始,PI这个宏就失效
#undef PI
    /*
     double d = PI;
     */
    return 0;
    
}