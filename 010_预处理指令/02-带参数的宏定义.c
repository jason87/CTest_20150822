#include <stdio.h>
/*
 预处理指令:
 1.在翻译之前执行的指令为预处理指令
 2.种类:
 1>宏定义(#define)
 2>条件编译
 3>文件包含(#include)
 3.特点
 1>都是以#开头
 2>指令后面不用加分号;
 3>作用域:从书写指令的那一行开始,一直到文件结尾
 4>书写的位置随意
 
 */

// 宏定义的结构: #define 宏名 值
// 宏替换:在翻译之前,将代码里面的所有count换成4
// 宏名最好用大写,跟变量名区分开来

// 带参数的宏定义
// 宏替换只是纯粹的文本替换,把左边的宏换成右边的东西,并不会进行任何运算
// 参数和结构都必须用()包住
#define PING_FANG(x) ((x)*(x))  // 把左边的换成右边的

int pingfang(int n){
    
    return n * n;
}


int main() {
    
    int a = 10;
    
    // int r = pingfang(a);
    int r = PING_FANG(a);
    printf("r is %d\n", r);
    
    return 0;
}