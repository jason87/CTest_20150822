#include <stdio.h>
#include "sum.h"
#include "minus.h"


/*
 1.不能递归包含: 自己包含自己 a.h包含a.h
 2.不能交叉包含: a.h包含b.h, b.h包含a.h
 3.防止.h文件的内容被包含多次
 
 #ifndef SUM_H(文件名)
 
 #define SUM_H(文件名)

 ..文件内容..
 
 #endif
 
 */
int main() {
    
    sum(10,10);
    
    return 0;
}