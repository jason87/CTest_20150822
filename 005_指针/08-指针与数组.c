
#include <stdio.h>


/*
 指针的p+1 跟指针所指向的数据类型有关
 
 char *     1;
 int *      8;
 double *   8;
 long *     8;
 
 */
int main() {
    

    char names[] = {'j', 'a'};
    
    char *p = names; //
    
    printf("%c\n", *(p + 1));
    
    double d = 10;
    
    double *dp = &d;
    
    printf("dp = %p\n", dp);
    printf("dp + 1 = %p\n", dp + 1);
    
    return 0;
    
}

/*
 p + 1的说明
 */
void test1() {
    
    int *p;
    
    int ages[2] = {10, 20};
    
    // 指针变量p指向了ages[0]这个元素
    p = &ages[0];
    
    // p = ages;
    
    // 指针变量的+1并不是基本数据类型中的简单+1
    // p + 1 究竟是增加多少,取决于指针的类型 地址值 + 4(int类型)
    //    p + 1;
    
    printf("&ages[0] == %p\n", &ages[0]); // 0x7fff50460a30
    printf("&ages[1] == %p\n", &ages[1]); // 0x7fff50460a34
    
    printf("p == %p\n", p);         // 0x7fff50460a30
    printf("p+1 == %p\n", p + 1);   // 0x7fff50460a34
    printf("%p\n", p + 2);          // 0x7fff50460a38
    
    
    
    /*
     int a = 10;
     
     a + 1; // 11
     
     */
    
}

