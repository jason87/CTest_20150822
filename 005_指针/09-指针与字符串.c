#include <stdio.h>

/*
 内存区域5块
    1.堆: 存放OC对象
    2.栈: 尊方的数值都是自动释放的
    3.常量区:  字符串常量(缓存)
 
 
 char *names = "jason"; // 存放在字符串常量区 不可改
 
 char names[] = "jason"; // 可改
 
 */
int main() {
    
    // 以前的字符串存储
    char name[] = "jason";
    char names[2][10] = {"jason", "helloworld"};
    
    // 使用指针以后的字符串存储
    char *name = "jason";
    // 指针数组 数组里面装的2个指针
    char *names[2] = {"jason", "helloworld"};
    
    
    
    
    return 0;
}

/*
    字符串常量
 */
void test2() {
    
    // names这个指针变量指向了字符串的首字符'j'
    // 直接利用指针变量指向的字符串常量,是不可变的
    char *names = "jason"; // 存放的是常量区
    
    //    printf("%s\n", names);
    
    /*  错误写法
     *(names + 4) = 'S';
     
     printf("%s\n", names);// Bus error: 10 程序崩溃了
     
     */
    
    char *names2 = "jason"; // 与上面的names是同一个地址值 与java字符串常量同理
    
    // names和names2指向的是同一个字符
    printf("names = %p\n", names);
    printf("names2 = %p\n", names2);
    
}


void test1() {
    
    // 利用数组存储的字符串是可变的
    char names[] = "jason";  // 存放的是栈里面
    
    char names2[] = "jason"; // 与上面的地址值不一样
    
    char *p = names;
    
    //    printf("%s\n", p + 2); // son
    
    *(p + 3) = 'A';
    
    printf("%s\n", names);
}