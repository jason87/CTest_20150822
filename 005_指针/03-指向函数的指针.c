#include <stdio.h>

/*
 1. 掌握"指向函数的指针"的定义
    double(int a, double d, char c){}
    double (*p)(int,double,char)
 2. 掌握利用指针变量间接调用函数
    p(10, 1.5 'a');
 
 */

void test() {
    
    printf("哈哈哈哈\n");
}

int sum(int v1, int v2) {
    
    return v1 + v2;
}


int minus(int v1, int v2) {
    
    return v1 - v2;
    
}

int multi(int v1, int v2) {
    
    return v1 * v2;
    
}

// 用来做整型变量a和b的一些算数运算(加减乘除)
int count(int a, int b, int (*p)(int, int)) {
    
    return p(a, b);
}


int main(){
    
//    int c = count(10, 11, sum);
    int c = count(10, 11, minus);
    
    printf("c is %d\n", c);
    
    return 0;
}


/*
 指向sum函数的指针
 */
void test2() {
    
    int (*p)(int,int) = sum;
    
    /*
     int (*p)(int,int);
     p = sum;
     等同于上面一句
     */
    
    int c = p(10,11);
    
    printf("%d\n", c);

}


/*
 指向test函数的指针
 */
void test1() {
    
    // 如果*p被()包住,说明指针变量p将来指向的是函数
    // 最左边的void说明p指向的函数没有返回值
    // 最右边的()说明p指向的函数并没有形参
    void (*p)();
    
    // 函数名test就是test函数的地址值
    // 将test函数的地址赋值给了指针变量p
    // 指针变量p成功指向了test函数
    p = test;
    
    // 利用指针变量p间接调用test函数
    //    (*p)();
    p();
    
}

