1. 指针变量的定义
int *p;
char *p2;

2. 利用指针间接操作其他变量的数据
1> 简单的使用
int a = 10;
int *p = &a;
*p = 11;

2> 在函数内部修改外面实参的值
int b = 10;
change(&b);
void change(int *n) {
    *n = 9;
}
-----------------------
int v1 = 10;
int v2 = 11;

swap(&v1, &v2);

void swap(int *a, int *b) {
    
    int temp = *a;
    *a = *b;
    *b = temp;
    
}

3> 利用指针实现函数的返回值
int v1 = 10;
int v2 = 11;
int sum; // 和
int minus// 差
sum = sum(v1,v2,&minus);

int sum(int a, int b, int *minus) {
    
    *minus = a - b;
    return a + b;
    
}




3.掌握 p+1的含义


4.掌握利用指针来存储字符串
char *name = "jason";
char *names[] = {"jason","rose"};

5.区别两种字符串的存储方式
1> char *name = "jason";
2> char name[] = "jason";








