#include <stdio.h>

/*
 1. 利用指针变量间接修改其他变量的值
 2. 知道怎样改变指针变量的指向
 3. 清空指针变量的指向 
 */

int main() {
    
    int a = 10;
    int b = 20;
    
    int *p;
    p = &a;
    p = &b;
    
    *p = 11;
    
    printf("a = %d\n b = %d\n", a, b);
    
    // 将指针p里面存储的地址值清空,这时候p就成空指针
    p = 0;
    // 其实NULL就是0
    p = NULL;
    
    return 0;
    
}