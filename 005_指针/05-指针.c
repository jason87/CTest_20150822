#include <stdio.h>

void change(int *);
void swap(int *,int *);
int sum(int, int, int *);


/*
  指针的作用: 只要你给指针一个地址, 它就能根据地址找到对应的存储空间 *p
 */

int main() {
    
    int a = 10;
    int b = 5;
    
    // 用来存放差的
    int cha;
    // 用来存放和的
    int he;
    
    he = sum(a, b, &cha);
    
    printf("he=%d\ncha=%d\n", he, cha);
    
    return 0;
}

/*
 设计一个函数, 计算两个整数的和 跟 差
 */
int sum(int v1, int v2, int *v3){
    
     *v3 = v1 - v2;
    
    return v1 + v2;
    
}


void swap(int *a, int *b) {
   
    // 将v1的值赋值给temp
    int temp = *a;
    // 将v2的值赋值给了v1
    *a = *b;
    // 将temp的值赋值给v2
    *b = temp;
    
}

void change(int *n) {
    
    *n = 10;
}


/*
 交换两个实参的值
 */
void test3() {
    int v1 = 10;
    int v2 = 11;
    
    // 通过函数来交换外面两个变量的值
    swap(&v1, &v2);
    
    printf("v1=%d \n v2=%d\n",v1 ,v2);
}


/*
 指针的使用
 */
void test2() {
    
    /*
     int a = 19;
     
     change(&a);
     
     printf("a的值是%d\n", a);
     */
    
    char c = 'A';
    
    char *p;
    // 说明指针变量p指向了c
    p = &c;
    
    *p = 'D';
    
    printf("c的值是%c\n", c);
    printf("c的值是%c\n", *p);
    
}


/*
  指针简单的使用
 */
void test1() {
    // 定义指针变量: 变量类型  *变量名;
    
    // 指针变量是用来 存储其他变量的地址
    int *p;
    // 定义一个整型变量
    int a = 10;
    
    // 将变量a的地址赋值给了指针变量p
    // 指针变量p指向了变量a
    p = &a;
    
    *p = 11;
    
    // 这里的*p意思是: 访问指针变量p指向的存储空间
    printf("a的值是%d\n",a); // 11

}