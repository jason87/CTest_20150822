
#include <stdio.h>

int main() {
    

    return 0;
    
}

void test5() {
    
    int ages[4] = {10, 11, 56, 47};
    
    int *p = ages;
    
    for (int i = 0; i < 4; i++) {
        
        printf("ages[%d]=%d\n", i, *p);
        
        p++;
        
    }
    // 这种遍历方式,指针的值一直在改变
    
}


/*
 指向其他元素
 */
void test4() {
    
    int ages[4] = {10, 11, 56, 47};
    
    int *p = &ages[1];
    
    // *(p+2)  *代表取出这个地址值的元素
    printf("%d\n",*(p+2)); // 47
    printf("%d\n", *p + 2); // 11 + 2 =  13
    
    
}

/*
 利用指针遍历数组元素2
 */
void test3() {
    int ages[4] = {10, 11, 56, 47};
    
    // p == 100 == ages == &ages[0]
    int *p = ages;
    
    // p + 1; // &ages[1]
    // p + 2; // &ages[2]
    // p + 3; // &ages[3]
    // p + i; // &ages[i]
    
    for (int i = 0; i < 4; i++) {
        
        int num = *(p + i);
        printf("ages[%d]=%d\n", i, num);
    }
    // 这种遍历方式 指针的值一直没变过
    
    printf("%d\n", *p); // 还是指向的10 说明指针指向的地址一直没有改变 是因为没有后面循环一直没有赋值

}

/*
 利用指针间接遍历数组元素
 */
void test2() {
    int ages[4] = {10, 11, 56, 47};
    
    //    for (int i  = 0; i < 4; i++) {
    //        printf("ages[%d]=%d\n", i , ages[i]);
    //    }
    
    int *p;
    // 指针变量p指向了ages[0]这个元素
    p = ages;
    
    for (int i = 0; i < 4; i++) {
        p = &ages[i];
        printf("ages[%d]=%d\n", i , *p);
        
    }
}


/*
 指向一维数组元素的指针
 */
void test1() {
    
    int ages[4] = {10, 11, 56, 47};
    
    // 定义一个指向int类型数据的指针
    int *p;
    // 指针变量p指向了ages[0]
    //    p = &ages[0];
    p = ages;
    *p = 70;
    
    printf("ages[0]=%d\n",ages[0]);
}