
#include <stdio.h>

/*
 1.任何指针变量都占用8个字节的存储空间
 2.指针类型只能指向对应类型的变量, 比如int *p只能指向int类型的数据
 */
int main() {
 
    // 0000 0000 0000 0000 0000 0000 0000 0010
    int i = 2;
    
    char c = 1;
    
    char *p = &c;
    
    printf("c is %d\n", *p);
    
    
    return 0;
    
}


/*
 一些小注意点
 */
void test1() {
    
    /*
     错误写法: 指针变量p没有进行初始化, 也就是说没有固定的指向空间
     float *p; // 未经初始化
     printf("%f\n", *p);
     */
    
    float f = 1.5f;
    /*
     float *p;
     p = &f;
     */
    /*
     这一句代码其实做了2件事
     1.定义了指针变量p
     2.让指针变量p指向了f
     */
    float *p = &f;
    
    printf("f is %f\n", *p);
    
    /*
     错误写法: 指针是用来存储地址的, 不要随便赋值一个常量
     float *p1;
     p1 = 100;
     
     */
    
    
    int *p1;
    // 只要是指针类型   占用8个字节
    printf("float *占用%ld \nint *占用%ld\n", sizeof(p), sizeof(p1));
}

