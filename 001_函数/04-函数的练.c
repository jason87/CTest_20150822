#include <stdio.h>
/*

 设计一个函数 计算1+2+3+4+5+.......+n的值
 要求n必须>0
*/

long leiJia (int n) {
    
    int sum = 0;
    /*
        sum += 1;
        sum += 2;
        sum += 3;
        ...
        sum += n;
     */
    
    for (int i = 1; i <= n; i++) {
        sum += i;
    }
    
    return sum;
    
}


/*
  递归思想的条件:
    1.  函数自己调用自己
    2.  函数必须有一个固定的返回值
    如果没有第2个条件,就会发生死循环
 
 */

// 递归思想实现
long leiJia2(int n) {
    if(n <= 0) return -1;
    
//    if(n == 1) return 1;
    
    return leiJia2(n -1) + n;
    
//    return (n<=0) ? 0 : leiJia2(n-1) + n ; 三元运算符也可完成
    
}


int main() {
    
    long result = leiJia(100);
    
    printf("%ld\n" , result);
    
    return 0;
}