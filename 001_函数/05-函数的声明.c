#include <stdio.h>



// 函数的声明: 只是说明一下这个函数是存在的(骗过编译器sum函数是存在的)
// 声明和定义的区别:是否有函数体
//long sum(int a, int b);
long sum(int,int);

void printfLine();

/*
 默认情况下,如果调用了main函数后面定义的函数,编译器直接报错.
 错误原因:因为main函数不知道所调用函数的存在
 */

// 定义 一个main函数
// 定义: 写清楚函数的具体代码
int main() {
    
    int d = sum(10, 11);
    printf("d is %i\n", d);
    
    printfLine();
    return 0;
    
}


/*
 warning: implicit declaration of function 'sum' is
 invalid in C99
 
 ANSI C
 C99
 C11
 sum函数没有明确定义
 
 C语言编译的时候会检测语法,检测语法的顺序是: 从上往下
 
 */

/*
 设计一个函数计算两个整数的和
 */

long sum(int a , int b) {
    
    return a+b;
}

void printfLine() {
    
    printf("-----------\n");
}


