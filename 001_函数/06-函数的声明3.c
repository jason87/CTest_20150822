

int printf(const char *s, ...);

int main() {
    
    printf("-------\n");
    
    return 0;
}

// 可以重新系统自带的函数 重写的话会完全覆盖系统的函数功能
int printf(const char *s, ...) {
    
    
    
    return 0;
}