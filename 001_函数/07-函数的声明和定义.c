


// #include 是预处理指令的一种,文件包含指令
// 预处理指令是在翻译之前执行的

#include <stdio.h>
#include "test.abc"
//int main() {
//    
//    printf("123\n");
//    return 0;
//}


/*
 1.编译
    1> 将stdio.h文件的内容拷贝到第7行的位置
    2> 将test.abc文件的内容拷贝到第8行的位置
    3> 将.c源文件翻译成0和1,如果编译成功就生成.o目标文件
 
 2.链接
    1> 将C语言函数库(函数的定义)跟.o目标文件合并在一起，生成一个可执行文件
 
 3.运行
    1> 执行a.out文件
 
 
 */

