
#include <stdio.h>

/*
 设计一个函数计算一个整数的n次方, 比如2的3次方, 就是8
 
 n必须 >= 0
 */

long power(int base, int n)
{
    // 如果n不符合条件,直接退出函数
    if (n<0) return -1;
    
    // base * base * base
    
    // 保存最后的结果
    long result = 1;
    
    for (int i = 1; i <= n; i++) {
        
        result *= base;
    }
    
    return result;
}

// 递归的方式实现
long power2(int base, int n) {
    
    if(n < 0) return -1;
    
    if(n == 0) return 1;
    
    return power2(base, n - 1) * base;
    
    
}


int main() {
    
    long l =  power2(3,3);
    
    printf("%ld\n",l);
    
}




