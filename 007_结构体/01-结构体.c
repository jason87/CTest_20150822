#include <stdio.h>

int main() {
    
    /*
     结构体 不能 递归包含
    struct Date {
        
        int year;
        int month;
        int day;
        
        struct Date dd;
        
    }
     */
    
    struct Date {
        
        int year;
        int month;
        int day;
        
    };
    
    struct Student {
        
        char *name;
        // 一个结构体可以引用别的结构体类型
        struct Date birthday;
    };
    
    // 定义变量
    struct Student stu1 = {"jason", {1987, 1, 10}};
    /*
     分别给内部的成员进行赋值
    stu1.birthday.year = 2015;
    stu1.birthday.month = 8;
    stu1.birthday.day = 30;
    */

    printf("name = %s\n", stu1.name);
    printf("birthday=%d-%d-%d\n",stu1.birthday.year, stu1.birthday.month,
           stu1.birthday.day);
    
    
    return 0;
}

/*
 结构体的各种定义方式
 */
void test2() {
    /*
     
     定义结构体类型
     struct 结构体名称
     {
     成员类型    成员名称1;
     成语类型    成员名称2;
     ......
     };
     
     */
    
    struct Record {
        
        int no; // 排名
        char *name; // 姓名
        int score; // 积分
        
    };
    
    struct Record person1 = {1, "jason", 10};
    struct Record person2 = {2, "rose", 8};
    
    struct Record person4 = {.name = "haha", .score = 7, .no = 4};
    
    //    struct Record person3 = {3, "hehe", 4}
    // 另外一种定义方式
    struct Record person3;
    person3.no = 3;
    person3.name = "jim";
    person3.score = 5;
    
    /*
     写法是错误的,只能在定义结构体变量的同时一次性赋值
     person3 = {3,"rose",6};
     */
    
    
    
}


/*
 结构体的定义
 */
void test1() {
    
    // int gaes[60]
    
    // 姓名  char *name = "jason";
    // 年龄 int age = 19;
    // 性别   char *sex = "男";
    // 身高 float height = 1.78f;
    
    /*
     1. 定义结构体类型（为系统增加一个新的类型）
     */
    
    struct Person
    {
        char *name;
        
        int age;
        
        char *sex;
        
        float height;
    };
    
    /*
     2. 定义结构体变量
     */
    
    struct Person p = {"jason", 19, "男", 1.78f};
    
    struct Person p2 = {"jack", 20, "女", 2.5f};
    
    
    printf("name=%s\n", p.name);
    printf("age=%d\n", p.age);
    printf("sex=%s\n", p.sex);
    printf("height=%f\n", p.height);

    
}


