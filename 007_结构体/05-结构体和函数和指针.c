#include <stdio.h>

struct Date {
    
    int year;
    int month;
    int day;
};

void test(struct Date date) {
    
    date.year = 2000;
}

/*
 函数里面修改结构体的属性
 */
void change(struct Date *date) {
    
    date->year = 2000;
    
}

int main() {
    
    /*
    struct Date mydate = {2015, 9, 1};
    
    // 结构体作为函数参数时,只是成员的值传递
    // 修改函数内部的结构体,并不影响外面的结构体
    test(mydate);
    printf("year=%d\n",mydate.year);
    
     */
    
    
    /*-------------------------------------*/
    
    
    /*
     指向结构体的指针
     
    struct Date mydate = {2015, 9, 1};

    struct Date *p;
    p = &mydate;
    
//    (*p).year = 2000;
    
    // 给指针变量p所指向的结构体的year成员赋值
    p->year = 2000;
    
    printf("year=%d\n",mydate.year);

     */
    
    struct Date mydate = {2015, 9, 1};
    change(&mydate);
    
    /*
     指向结构体类型的指针
    struct Point {
      
        double x;
        double y;
        
    } *p1;
    */
     
     
    return 0;
}
