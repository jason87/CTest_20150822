#include <stdio.h>

int main() {
    
    struct Record {
        
        int no; // 排名
        char *name; // 名称
        int score; // 分数
        
    };
    
    // 定义了一个结构体数组
    struct Record records[3] = {
        
        {1, "jason", 10},
        {2, "rose", 9},
        {3, "jim", 5}
    };
    
    // 访问下标为1的结构体的score属性
    printf("score=%d\n", records[1].score);
    
    /*
     错误写法: 如果想给结构体一次性赋值,只能在定义变量的同时
    records[1] = {4, "haha", 100};
    
     */
    
    struct Record newRecord = {4,"haha",100};
    records[1] = newRecord;
    
    return 0;
}