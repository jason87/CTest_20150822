#include <stdio.h>


// 将结构体类型定义在函数外面,作用域相当于全局变量
struct Date {
    
    int year;
    int month;
    int day;
    
};

// 全局变量
struct Date d3;

void test() {
    
//    struct Date {
//        
//        int year;
//        int month;
//        int day;
//        
//    };
    struct Date date;
    date.year = 2015;
    
    
}

int main() {
    
    test();
    
    struct Date d1;
    d1.year = 2017;
    
    struct Point {
        
        double x;
        double y;
        
    };
    
    struct Size {
        
        double width;
        double height;
        
    };
    
    struct Bounds {
      
        struct Point position;
        struct Size size;
        
    };
    
    
    
    
    return 0;
}