#include <stdio.h>

int main() {

    struct Student {
        
        char *name; //8
        int no; // 4
        int age; // 4
    };
    
    struct Student stu1;
    // 默认情况下,一个结构体变量所占用的存储看空间是所有成员的总和
    // sizeof计算结构体所占用存储空间时,返回的值必须是最大成员的倍数
    int length = sizeof(stu1);
    
    printf("length=%d\n",length);
    
    return 0;
}

/*
 定义结构体的三种方式
 */
void test4() {
    
    struct Student {
        
        
    };
    
    struct Student s1;
    
    
    struct Date {
        
    } d;
    
    
    struct {
        
    } c;
}


/*
 结构体变量的定义: 省略了结构名称,这种方式没有重用性
 */
void test3() {
    
    // 在定义结构体类型的同时定义一个变量
    struct {
        
        double width;
        double height;
        
    } s;
    
    struct {
        
        double width;
        double height;
        
    } s2;
}

/*
 结构体变量的定义:定义类型的同时定义一个变量
 */

void test2() {
    
    
    // 这条语句做了2件事: 1>定义了结构体类型  2>定义了结构体变量s
    struct Size {
        
        double width;
        double height;
        
    } s;
    
    struct Size s2 = {100.0, 50.0};
}


void test1() {
    
    // 为程序添加一种新的类型
    struct Point {
        
        double x;
        double y;
        
    };
    
    struct Point p = {10.6, 20.7};
    
    // 将结构体变量p所有成员的值都赋值给了p2的成员
    struct Point p2 = p;
    
    p2.x = 10;
    
    printf("p2.x = %f\n", p2.x);// 10
    
    printf("p.x = %f\n", p.x);
    
}